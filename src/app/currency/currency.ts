export class Currency {

  currencyId: string;
  currencyDesc: string;
  currencyDescArabic: string;
  currencySymbol: string;
  isIncludeSpace: boolean;
  negativeSymbol: number;
  displaySymbol: number;
  decimalSymbol: number;
  thousandSymbol: number;
  currencyUnit: string;
  subUnitConnector: string;
  currencySubUnit: string;
  currencyUnitArabic: string;
  subUnitConnectorArabic: string;
  currencySubUnitArabic: string;


  constructor(currencyId: string, currencyDesc: string,
  			  currencyDescArabic: string, currencySymbol: string,
  			  isIncludeSpace: boolean, negativeSymbol: number,
  			  displaySymbol: number, decimalSymbol: number,
  			  thousandSymbol: number, currencyUnit: string,
  			  subUnitConnector: string, currencySubUnit: string,
			  currencyUnitArabic: string, subUnitConnectorArabic: string,
  			  currencySubUnitArabic: string) {

		this.currencyId = currencyId;
		this.currencyDesc =currencyDesc;
		this.currencyDescArabic = currencyDescArabic;
		this.currencySymbol = currencySymbol;
		this.isIncludeSpace = isIncludeSpace;
		this.negativeSymbol = negativeSymbol;
		this.displaySymbol = displaySymbol;
		this.decimalSymbol = decimalSymbol;
		this.thousandSymbol = thousandSymbol;
		this.currencyUnit = currencyUnit;		
		this.subUnitConnector = subUnitConnector;
		this.currencySubUnit = currencySubUnit;
		this.currencyUnitArabic = currencyUnitArabic,
		this.subUnitConnectorArabic = subUnitConnectorArabic;
  		this.currencySubUnitArabic = currencySubUnitArabic

  }

}


