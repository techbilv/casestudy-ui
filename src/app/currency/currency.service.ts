import { Injectable } from '@angular/core';
import { Currency } from "./currency";
import { Http, Response } from "@angular/http";
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { Observable } from "rxjs/Observable";

@Injectable()
export class CurrencyService {

  private apiUrl = '/api/v1/currency';
  
  constructor(private http: Http) {
  }

  getCurrencies() {
    return this.http.get(this.apiUrl)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  
  createCurrency(currency : Currency) {
        return this.http.post(this.apiUrl, currency)
            .map((response: Response) => response.json());
    }
}
