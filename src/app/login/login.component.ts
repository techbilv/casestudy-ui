import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 
import { Login } from "./login";
import { LoginService } from "./login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  model: any = {};
  status: any = {};
  
  constructor(
  		private loginService: LoginService,
  		private router : Router) { }

  ngOnInit() {
  	  
  }

  login() {
        this.loginService.login(this.model.email, this.model.password)
            .subscribe(
                data => {
                	this.router.navigate(['/currency/list']);
                },
                error => {
        			this.status.type ='error';
        			this.status.prefix = 'Error!';
        			this.status.message = error.json().detail;
                });
    }
}